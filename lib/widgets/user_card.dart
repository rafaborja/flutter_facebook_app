import 'package:facebook_app_excercise/models/models.dart';
import 'package:facebook_app_excercise/widgets/widgets.dart';
import 'package:flutter/material.dart';

class UserCard extends StatelessWidget {
  final User user;

  const UserCard({Key key, this.user}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){},
      child: Row(
        mainAxisSize: MainAxisSize.min,
          children: [
            ProfileAvatar(imageUrl: user.imageUrl, isActive: false),
            SizedBox(width: 6.0),
            Flexible(child: Text(user.name, style: TextStyle(fontSize: 16.0), overflow: TextOverflow.ellipsis))
          ],
      ),
    );
  }
}